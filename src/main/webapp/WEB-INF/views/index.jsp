<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home</title>
</head>
<body>
	<div align="center">
		<form:form>
			<table border="0">
				<tr>
					<td colspan="2" align="center"><h1>Welcome to Register Here</h1></td>
				</tr>
				<tr>
					<td><a href="${pageContext.request.contextPath}/register">Register</a></td>
					<td><a href="${pageContext.request.contextPath}/addCountry">Add Country</a></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>
