<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Success</title>
<style>
#customers {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

#customers td, #customers th {
	border: 1px solid #ddd;
	padding: 8px;
}

#customers tr:nth-child(even) {
	background-color: #f2f2f2;
}

#customers tr:hover {
	background-color: #ddd;
}

#customers th {
	padding-top: 12px;
	padding-bottom: 12px;
	text-align: left;
	background-color: #4CAF50;
	color: white;
}
</style>
</head>
<body>
	<table border="0">
		<tr>
			<td colspan="2" align="center"><h2>Spring MVC Form Demo </h2></td>
		</tr>
		<tr>
			<td>Country Code:</td>
			<td>${country.countryCode}</td>
		</tr>
		<tr>
			<td>Country Name:</td>
			<td>${country.countryName}</td>
		</tr>
		
		<tr>
			<td><a href="${pageContext.request.contextPath}/">Go Home</a></td>
		</tr>
	</table>

	<br>
	<h3>Country List</h3>
	<c:if test="${!empty countryList}">
		<table id="customers">
		<tr>
				<th>Country Code</th>
				<th>Country Name</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
			<c:forEach items="${countryList}" var="country">
				<tr>
					<td>${country.countryCode}</td>
					<td>${country.countryName}</td>
					<td><a href="<c:url value='/edit/${country.id}' />">Edit</a></td>
					<td><a href="<c:url value='/remove/${country.id}' />">Delete</a></td>
				</tr>
			</c:forEach>
	</table>
	</c:if>

	

</body>
</html>