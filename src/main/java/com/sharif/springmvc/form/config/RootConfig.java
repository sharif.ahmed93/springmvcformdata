package com.sharif.springmvc.form.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.sharif.springmvc.config","com.sharif.springmvc.services"})
public class RootConfig {

}
