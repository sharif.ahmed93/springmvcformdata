package com.sharif.springmvc.form.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.persistence.Entity;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.reflections.Reflections;

import com.mysql.cj.Session;

import javassist.Modifier;

public class HibernateConfig {

	private SessionFactory sessionFactory = null;
	private Session session = null;

	public Session getSession() {
		return null;
	}

	private SessionFactory createAndGetLocalSessionFactoryBean() {
		if (this.sessionFactory == null) {
			try {
				Configuration configuration = new Configuration();
				Properties properties = getProperties("hibernate.properties");
				configuration.setProperties(properties);
				configuration.addPackage("com.sharif.springmvc.form.model");
				for (Class<?> modelClass : (new Reflections("com.sharif.springmvc.form.model"))
						.getTypesAnnotatedWith(Entity.class)) {

					if (!Modifier.isAbstract(modelClass.getModifiers())) {
						configuration.addAnnotatedClass(modelClass);
					}

				}

				StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder()
						.applySettings(properties);
				sessionFactory = configuration.buildSessionFactory(serviceRegistryBuilder.build());

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return sessionFactory;
	}

	private Properties getProperties(String name) {
		Properties properties = new Properties();
		InputStream input = Hibernate.class.getClassLoader().getResourceAsStream(name);
		try {
			properties.load(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return properties;
	}
}
