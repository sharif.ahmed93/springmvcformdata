package com.sharif.springmvc.form.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.sharif.springmvc.form.model.Country;
import com.sharif.springmvc.form.model.User;

@Controller
public class CountryController {

	@GetMapping("/addCountry")
	public String viewAddCountryPage(Model model) {
		Country country = new Country();
		model.addAttribute("country",country);
		return "add-country";
	}
	
	@PostMapping("/addCountry")
	public String addCountry(Model model, @ModelAttribute("country") Country country) {

		List<Country>countryList = new ArrayList<Country>();
		countryList.add(country);
		// implement your own registration logic here...


		model.addAttribute("country", country);
		model.addAttribute("countryList", countryList);
		return "country-success";
	}
}
