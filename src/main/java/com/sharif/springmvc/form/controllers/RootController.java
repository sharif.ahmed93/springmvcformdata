package com.sharif.springmvc.form.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RootController {

	@GetMapping("/")
	public String index() {
		return "index";		
	}
}
